
/*
	Mini-Activity:

 	1. Create an "activity" folder, an "index.html" file inside of it and link the "script.js" file.
	2. Create a "script.js" file and console log the message "Hello World" to ensure that the script file is properly associated with the html file.
	3. Create a "trainer" object by using object literals.
	4. Initialize/add the "trainer" object properties.
	5. Initialize/add the "trainer" object method.
	6. Access the "trainer" object properties using dot and square bracket notation.
	7 .Invoke/call the "trainer" object method.
	8. Create a constructor function for creating a pokemon.
	9. Create/instantiate several pokemon using the "constructor" function.
	10. Have the pokemon objects interact with each other by calling on the "tackle" method.

 */

document.getElementById("title").innerHTML = "Hello World"
console.log('HELLO WORLD');

let trainer = {
	name: 'RED', 
	weight: '115 grams', 
	height: "5'3",
	gymBadge:3,
	talk:function(){
		console.log("I'll be the best pokemon trainer.");
	}
};

trainer.gymBadge = 5;
console.log(trainer.gymBadge);
trainer.talk = function(){
	console.log("I'll will be the best the no one ever was.");
}
console.log(trainer.talk);
console.log(trainer);

function Pokemon(name){
	this.name = name; 
	this.health = 100; 
	this.attack = function(target){
		console.log(`${this.name} used Spacial Rend on ${target.name}`);
		target.health -= 50;
		console.log(`${target.name}'s health is now ${target.health}`);
		console.log(`${target.name} is turn to attack`);
		console.log(`${target.name} used Shadow Force on ${this.name}`);
		this.health -= 100;
		console.log(`${this.name}'s health is now ${this.health}, ${this.name} failed. ${target.name} won,Trainer Red Obtain $250`);

	}
}

let palkia = new Pokemon("Palkia");
let giratina = new Pokemon("Giratina");

palkia.attack(giratina);